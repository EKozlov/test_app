using Serilog;
using Serilog.Sinks.Elasticsearch;
using TestCase.Web.Api.Extensions;
using TestCase.Web.Api.Middleware;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.ConfigureLogging();
builder.Host.UseSerilog((context, services, configuration) =>
				configuration.Enrich.FromLogContext()
				.Enrich.WithMachineName()
				.WriteTo.Console()
				.WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(context.Configuration["ElasticConfiguration:Uri"]))
				{
					IndexFormat = $"{context.Configuration["ElasticConfiguration:Index"]}{DateTime.UtcNow:yyyy-MM}",
					AutoRegisterTemplate = true,
					NumberOfShards = 2,
					NumberOfReplicas = 1,
				}).ReadFrom.Configuration(context.Configuration)
				.ReadFrom.Services(services));
builder.Services.AddServices(builder.Configuration);
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();


if (app.Environment.IsDevelopment())
{
	app.UseSwagger();
	app.UseSwaggerUI();
}

app.UseCors(corsPolicyBuilder =>
{
	// ����� whitelist ��������� ������� � ��. ��� ���� ������� ����� � �����
	corsPolicyBuilder.AllowAnyOrigin()
	.AllowAnyMethod()
	.AllowAnyHeader();
});

app.UseMiddleware<ExceptionHandlerMiddleware>();

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();

app.Run();
