﻿using System.Net.Mime;
using TestCase.Exceptions;

namespace TestCase.Web.Api.Middleware
{
	internal sealed class ExceptionHandlerMiddleware : IMiddleware
	{
		public async Task InvokeAsync(HttpContext context, RequestDelegate next)
		{
			try
			{
				await next.Invoke(context);
			}
			catch (BaseException ex)
			{
				var logger = context.RequestServices.GetRequiredService<Serilog.ILogger>();
				
				var hostingEnvironment = context.RequestServices.GetRequiredService<IHostEnvironment>();

				ex.WriteToLog(logger);

				context.Response.StatusCode = (int)ex.StatusCode;

				context.Response.ContentType = MediaTypeNames.Application.Json;

				await context.Response.StartAsync(context.RequestAborted);

				ex.Serialize(context.Response.BodyWriter, hostingEnvironment.IsProduction());

				var result = await context.Response.BodyWriter.FlushAsync(context.RequestAborted);

				if (result.IsCanceled)
					throw new OperationCanceledException(context.RequestAborted.IsCancellationRequested
						? context.RequestAborted
						: new CancellationToken(true));
			}
		}
	}
}
