﻿using MediatR;
using MediatR.Pipeline;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OpenIddict.Validation.AspNetCore;
using Serilog;
using System.Reflection;
using TestCase.Authorization.Token.Provider;
using TestCase.Authorization.Token.TokenHandlers.Base;
using TestCase.Authorization.Token.TokenHandlers.TestAppTokenHandler;
using TestCase.DataAccess;
using TestCase.Domain.Entities.OpenIdict;
using TestCase.Web.Api.Configuration;
using TestCase.Web.Api.HostedServices.Oidc;
using TestCase.Web.Api.Logging;
using TestCase.Web.Api.Middleware;

namespace TestCase.Web.Api.Extensions
{
	public static class ServiceCollectionExtensions
	{
		private static readonly List<Assembly> AllAssemblies = new();
		public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
		{
			services.Configure<OidcSettings>(configuration.GetSection(OidcSettings.OidcConfigurationName));
			services.AddDbContext<ApplicationDbContext>(options =>
			{

				options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
				options.UseOpenIddict();
			});
			services.AddTransient<ExceptionHandlerMiddleware>();
			services.AddScoped<DbContext, ApplicationDbContext>();

			LoadAssemblies();

			services.AddMediatR()
				.AddTokenHandlers()
				.AddOIddict(configuration)
				.AddAutoMapper();

			return services;
		}

		private static IServiceCollection AddTokenHandlers(this IServiceCollection services)
        {
			services.AddAuthentication(options =>
			{
				options.DefaultScheme = OpenIddictValidationAspNetCoreDefaults.AuthenticationScheme;

			});

			services.AddTransient<IPasswordHasher<Domain.Entities.User>, Authorization.Token.PasswordHasher>();
			services.AddTransient<ITokenHandler, TestAppTokenHandler>();
            services.AddTransient<IAccessTokenClaimsProvider, AccessTokenClaimsProvider>();
            return services;
		}

		private static IServiceCollection AddOIddict(this IServiceCollection services, IConfiguration configuration)
		{
			var oidcSettings = configuration.GetSection(OidcSettings.OidcConfigurationName).Get<OidcSettings>();

			services.AddIdentity<Domain.Entities.User, Role>(options =>
			{
				options.ClaimsIdentity.RoleClaimType = "role";
			})
			.AddEntityFrameworkStores<ApplicationDbContext>()
			.AddDefaultTokenProviders();
			services.Configure<IdentityOptions>(options =>
			{
				options.Password.RequireDigit = false;
				options.Password.RequireLowercase = false;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequireUppercase = false;
				options.Password.RequiredLength = 1;
				options.Password.RequiredUniqueChars = 0;
			});

			services.AddAuthentication(options =>
			{
				options.DefaultAuthenticateScheme = "OpenIddict.Validation.AspNetCore";
			});

			services.AddOpenIddict()
				.AddCore(options =>
				{
					options.UseEntityFrameworkCore()
					   .UseDbContext<DbContext>()
					   .ReplaceDefaultEntities<OpenIddictApplication,
						   OpenIddictAuthorization, OpenIddictScope, OpenIddictToken, string>();

					options.UseQuartz();
				})
				.AddServer(options =>
				{
					options.SetTokenEndpointUris("/api/identity/token");

					options.AllowPasswordFlow();

					options.AllowRefreshTokenFlow();

					options.SetAccessTokenLifetime(TimeSpan.FromMinutes(oidcSettings.AccessTokenLifeTimeMinutes));

					options.SetRefreshTokenLifetime(TimeSpan.FromDays(1));

					options.SetRefreshTokenReuseLeeway(TimeSpan.FromDays(1));

					options.AddDevelopmentEncryptionCertificate()
						.AddDevelopmentSigningCertificate();

					
					options.RegisterScopes("test_app");

					options.UseAspNetCore()
						.EnableAuthorizationEndpointPassthrough()
						.EnableTokenEndpointPassthrough()
						.EnableUserinfoEndpointPassthrough()
						.DisableTransportSecurityRequirement();
					options.DisableAccessTokenEncryption();
				})
				.AddValidation(options =>
				{
					options.UseLocalServer();
					options.UseAspNetCore();
				});
			services.AddHostedService<OidcInitializer>();
			return services;
		}

		private static IServiceCollection AddMediatR(this IServiceCollection services)
		{
			services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
			services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPostProcessorBehavior<,>));
			services.AddTransient(typeof(IRequestPreProcessor<>), typeof(LoggingPreProcessBehaviour<>));
			services.AddTransient(typeof(IRequestPostProcessor<,>), typeof(LoggingPostProcessBehaviour<,>));

			var types = AllAssemblies
				.Where(x => x.FullName != null
							&& x.FullName.Contains("TestCase"))
				.SelectMany(x => x.GetTypes())
				.Where(type => type.GetInterfaces().Any(interfaceType =>
					interfaceType.IsGenericType &&
					interfaceType.GetGenericTypeDefinition() == typeof(IRequestHandler<,>)))
				.ToArray();

			services.AddMediatR(types);
			return services;
		}
		private static void LoadAssemblies()
		{
			var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies()
				.Where(p => !p.IsDynamic)
				.ToList();
			var loadedPaths = loadedAssemblies
				.Select(a => a.Location)
				.ToArray();

			var referencedPaths = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll");
			var toLoad = referencedPaths
				.Where(r => !loadedPaths.Contains(r, StringComparer.InvariantCultureIgnoreCase))
				.ToList();

			toLoad.ForEach(path =>
			{
				loadedAssemblies.Add(AppDomain.CurrentDomain.Load(AssemblyName.GetAssemblyName(path)));
			});

			AllAssemblies.AddRange(loadedAssemblies);
		}

		private static void AddAutoMapper(this IServiceCollection services)
		{
			var autoMapperAssemblies = AllAssemblies
				.Where(x => x.FullName != null
							&& !x.FullName.Contains("Microsoft")
							&& !x.FullName.Contains("System")
							&& x.FullName.Contains("TestCase")
							&& !x.FullName.Contains("AutoMapper"))
				.SelectMany(x => x.GetTypes())
				.Where(x => !x.IsInterface)
				.Where(type => typeof(AutoMapper.Profile).IsAssignableFrom(type))
				.ToArray();

			services.AddAutoMapper(autoMapperAssemblies);
		}

		public static void ConfigureLogging(this IConfiguration configuration)
		{
			Log.Logger = new LoggerConfiguration()
				.Enrich.FromLogContext()
				.Enrich.WithMachineName()
				.ReadFrom.Configuration(configuration)
				.CreateBootstrapLogger();
		}
	}
}
