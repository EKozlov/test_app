﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using OpenIddict.Abstractions;
using OpenIddict.Server.AspNetCore;
using System.Security.Claims;
using TestCase.Authorization.Token.TokenHandlers.Base;
using TestCase.Authorization.Token.TokenHandlers.TestAppTokenHandler;
using TestCase.Resources;
using static OpenIddict.Abstractions.OpenIddictConstants;

namespace TestCase.Web.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class IdentityController : ControllerBase
	{
		private readonly IOpenIddictApplicationManager _applicationManager;
		private readonly IEnumerable<ITokenHandler> _tokenHandler;
		public IdentityController(IOpenIddictApplicationManager applicationManager, IEnumerable<ITokenHandler> tokenHandler)
		{
			_applicationManager = applicationManager;
			_tokenHandler = tokenHandler;
		}

		[HttpPost("token"), Produces("application/json")]
		public async Task<IActionResult> Token()
		{
			var request = HttpContext.GetOpenIddictServerRequest();

			var application = await _applicationManager.FindByClientIdAsync(request.ClientId) ??
				throw new InvalidOperationException(ExceptionMessages.ApplicationNotFound);

			foreach (var tokenHandler in _tokenHandler)
			{
				var handleResult = await tokenHandler.Handle(request);

				if (handleResult.IsT2)
				{
					continue;
				}

				if (handleResult.IsT0)
				{
					var errorResponse = handleResult.AsT0;

					return Forbid(
						authenticationSchemes: OpenIddictServerAspNetCoreDefaults.AuthenticationScheme,
						properties: new AuthenticationProperties(new Dictionary<string, string>
						{
							[OpenIddictServerAspNetCoreConstants.Properties.Error] = errorResponse.Message
						}!));
				}

				if (handleResult.IsT1)
				{
					return SignIn(handleResult.AsT1, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
				}


			}

			throw new InvalidOperationException(ExceptionMessages.NotSupportedGrant);
		}
	}
}
