﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using TestCase.Authorization.Token.TokenHandlers.Base;
using TestCase.Exceptions;
using TestCase.User.Queries.GetCountries;
using TestCase.User.RegisterUser;

namespace TestCase.Web.Api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UserController : ControllerBase
	{
		private readonly IMediator _mediator;
		public UserController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// Register user with two steps.
		/// </summary>
		/// <param name="request">Register user request</param>
		/// <param name="token">Cancellation token</param>
		/// <returns></returns>
		[ProducesResponseType(typeof(Guid), StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(ErrorMessage), StatusCodes.Status400BadRequest)]
		[ProducesResponseType(typeof(OpenIdctError), StatusCodes.Status401Unauthorized)]
		[HttpPost("register")]
		public async Task<IActionResult> RegisterUser(RegisterUserRequest request, CancellationToken token)
			=> Ok(await _mediator.Send(request, token));

		/// <summary>
		/// Get countries with provinces
		/// </summary>
		/// <param name="token">Cancellation token</param>
		/// <returns></returns>
		[ProducesResponseType(typeof(IReadOnlyCollection<GetCountriesQueryResponse>), StatusCodes.Status200OK)]
		[ProducesResponseType(typeof(OpenIdctError), StatusCodes.Status401Unauthorized)]
		//Закомментил из-за ненадобности, данный кейс не описан в задании, но функционал работает
		//[Authorize]
		[HttpGet("countries")]
		public async Task<IActionResult> GetCountries(CancellationToken token)
			=> Ok(await _mediator.Send(new GetCountriesQuery(), token));


	}
}
