﻿namespace TestCase.Web.Api.Configuration
{
	public partial class Client
	{
		public string ClientId { get; set; }
		public string DisplayName { get; set; }
		public string ClientSecret { get; set; }
		public IReadOnlyCollection<string> Permissions { get; set; }
	}
}
