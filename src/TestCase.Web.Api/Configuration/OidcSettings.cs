﻿namespace TestCase.Web.Api.Configuration
{
	public class OidcSettings
	{
		public const string OidcConfigurationName = "oidc";
		public int AccessTokenLifeTimeMinutes { get; set; }
		public IReadOnlyCollection<Client> Clients { get; set; }
		public IReadOnlyCollection<Scope> Scopes { get; set; }
	}
}
