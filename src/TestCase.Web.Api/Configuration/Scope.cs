﻿namespace TestCase.Web.Api.Configuration
{
	public class Scope
	{
		public string Name { get; set; }
		public IReadOnlyCollection<string> Resources { get; set; }
	}
}
