﻿using OpenIddict.Abstractions;
using TestCase.DataAccess;
using TestCase.Web.Api.Configuration;

namespace TestCase.Web.Api.HostedServices.Oidc
{
	public class OidcInitializer : IHostedService
	{
		private readonly IServiceProvider _serviceProvider;
		public OidcInitializer(IServiceProvider serviceProvider)
		{
			_serviceProvider = serviceProvider;
		}

		private static async Task AddScopesAsync(
			IServiceScope serviceScope,
			OidcSettings settings,
			CancellationToken token)
		{
			var scopeManager = serviceScope.ServiceProvider.GetRequiredService<IOpenIddictScopeManager>();

			if (scopeManager is null)
				throw new InvalidOperationException();

			foreach (var scope in settings.Scopes)
			{
				if (await scopeManager.FindByNameAsync(scope.Name, token) == null)
				{
					var descriptor = new OpenIddictScopeDescriptor
					{
						Name = scope.Name,
					};

					descriptor.Resources.UnionWith(scope.Resources);

					await scopeManager.CreateAsync(descriptor, token);
				}
			}
		}
		private static async Task AddClientsAsync(
			IServiceScope scope,
			OidcSettings settings,
			CancellationToken token)
		{
			var manager = scope.ServiceProvider.GetRequiredService<IOpenIddictApplicationManager>();

			if (manager is null)
				throw new InvalidOperationException();

			if (!settings.Clients.Any())
				return;

			foreach (var client in settings.Clients)
			{
				if (await manager.FindByClientIdAsync(client.ClientId, token) is null)
				{
					var clientDescriptor = new OpenIddictApplicationDescriptor
					{
						ClientId = client.ClientId,
						ClientSecret = client.ClientSecret,
						DisplayName = client.DisplayName,
					};

					clientDescriptor.Permissions.UnionWith(client.Permissions);

					await manager.CreateAsync(clientDescriptor, token);
				}
			}
		}

		public async Task StartAsync(CancellationToken cancellationToken)
		{
			using var scope = _serviceProvider.CreateScope();

			var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

			await context.Database.EnsureCreatedAsync(cancellationToken);

			var configuration = scope.ServiceProvider.GetRequiredService<IConfiguration>();

			var settings = configuration.GetSection(OidcSettings.OidcConfigurationName)
				.Get<OidcSettings>();

			await AddClientsAsync(scope, settings, cancellationToken);
			await AddScopesAsync(scope, settings, cancellationToken);
		}
		public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
	}
}
