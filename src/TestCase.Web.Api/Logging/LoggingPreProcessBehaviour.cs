﻿using MediatR.Pipeline;
using Newtonsoft.Json;
using TestCase.Resources;
using ILogger = Serilog.ILogger;

namespace TestCase.Web.Api.Logging
{
	internal class LoggingPreProcessBehaviour<TRequest> : IRequestPreProcessor<TRequest>
	{
		private readonly ILogger _logger;
		public LoggingPreProcessBehaviour(ILogger logger)
		{
			_logger = logger;
		}
		public Task Process(TRequest request, CancellationToken cancellationToken)
		{
			_logger.Information(string.Format(LogMessages.HandlerStartedLog, typeof(TRequest).Name,
				JsonConvert.SerializeObject(request)));

			return Task.CompletedTask;
		}
	}
}
