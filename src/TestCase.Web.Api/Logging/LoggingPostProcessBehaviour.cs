﻿using MediatR;
using MediatR.Pipeline;
using Newtonsoft.Json;
using TestCase.Resources;
using ILogger = Serilog.ILogger;

namespace TestCase.Web.Api.Logging
{
	public class LoggingPostProcessBehaviour<TRequest, TResponse> : IRequestPostProcessor<TRequest, TResponse>
		where TRequest : IRequest<TResponse>
	{
		private readonly ILogger _logger;

		public LoggingPostProcessBehaviour(ILogger logger)
		{
			_logger = logger;
		}

		public Task Process(TRequest request, TResponse response, CancellationToken cancellationToken)
		{
			_logger.Information(string.Format(LogMessages.HandlerFinishedLog, typeof(TRequest).Name,
				JsonConvert.SerializeObject(request), typeof(TResponse).Name, JsonConvert.SerializeObject(response)));

			return Task.CompletedTask;
		}
	}
}