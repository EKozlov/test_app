﻿using MediatR;

namespace TestCase.User.Queries.GetUser
{
	public class GetUserQuery : IRequest<GetUserQueryResponse>
	{
		public Guid UserId { get; set; }
	}
}
