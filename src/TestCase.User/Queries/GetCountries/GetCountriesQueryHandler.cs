﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace TestCase.User.Queries.GetCountries
{
	public class GetCountriesQueryHandler : IRequestHandler<GetCountriesQuery, IReadOnlyCollection<GetCountriesQueryResponse>>
	{
		private readonly DbContext _db;
		private readonly IMapper _mapper;
		public GetCountriesQueryHandler(DbContext db, IMapper mapper)
		{
			_db = db;
			_mapper = mapper;
		}
		public async Task<IReadOnlyCollection<GetCountriesQueryResponse>> Handle(GetCountriesQuery request, CancellationToken cancellationToken)
		{
			var countries = await _db.Set<Domain.Entities.Country>()
				.Include(x => x.Provinces)
				.ToArrayAsync(cancellationToken);

			var mapEntity = _mapper.Map<IReadOnlyCollection<GetCountriesQueryResponse>>(countries);

			return mapEntity;
		}
	}
}
