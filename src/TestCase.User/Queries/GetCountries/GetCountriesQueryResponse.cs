﻿using TestCase.Domain.Entities;

namespace TestCase.User.Queries.GetCountries
{
	public class GetCountriesQueryResponse
	{
		public string Name { get; set; }
		public Guid Id { get; set; }
		public IEnumerable<Province> Provinces { get; set; }
	}
}
