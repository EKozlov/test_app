﻿using MediatR;

namespace TestCase.User.Queries.GetCountries
{
	public class GetCountriesQuery : IRequest<IReadOnlyCollection<GetCountriesQueryResponse>>
	{
	}
}
