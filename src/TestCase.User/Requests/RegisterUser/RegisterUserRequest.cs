﻿using MediatR;

namespace TestCase.User.RegisterUser
{
	public class RegisterUserRequest : IRequest<Guid>
	{
		public string Email { get; set; }
		public string Password { get; set; }
		public Guid CountryId { get; set; }
		public Guid ProvinceId { get; set; }
	}
}
