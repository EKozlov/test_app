﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TestCase.Exceptions.BadRequest;
using TestCase.Resources;

namespace TestCase.User.RegisterUser
{
	public class RegisterUserRequestHandler : IRequestHandler<RegisterUserRequest, Guid>
	{
		private readonly DbContext _db;
		private readonly IMapper _mapper;
		private readonly UserManager<Domain.Entities.User> _userManager;

		public RegisterUserRequestHandler(DbContext db, IMapper mapper, UserManager<Domain.Entities.User> userManager)
		{
			_db = db;
			_mapper = mapper;
			_userManager = userManager;
		}

		public async Task<Guid> Handle(RegisterUserRequest request, CancellationToken cancellationToken)
		{
			var user = await _db.Set<Domain.Entities.User>()
				.FirstOrDefaultAsync(x => x.Email == request.Email, cancellationToken);

			if (user is not null)
				throw new UserAlreadyExistBadRequestException(string.Format(request.Email, ExceptionMessages.UserAlreadyExist));

			var mapEntity = _mapper.Map<Domain.Entities.User>(request);
			
			mapEntity.HashPassword(request.Password);

			await _userManager.CreateAsync(mapEntity);

			return mapEntity.Id;
		}
	}
}
