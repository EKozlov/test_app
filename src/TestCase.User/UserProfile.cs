﻿using AutoMapper;
using TestCase.User.Queries.GetCountries;
using TestCase.User.RegisterUser;

namespace TestCase.User
{
	public class UserProfile : Profile
	{
		public UserProfile()
		{
			CreateMap<RegisterUserRequest, Domain.Entities.User>()
				.ForMember(dest => dest.UserName, opt => opt.MapFrom(x => x.Email));

			CreateMap<Domain.Entities.Country, GetCountriesQueryResponse>();

		}
	}
}
