﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestCase.Domain.Entities;

namespace TestCase.DataAccess.Configurations
{
	public class UserConfiguration : IEntityTypeConfiguration<User>
	{
		public void Configure(EntityTypeBuilder<User> builder)
		{
			builder.HasKey(x => x.Id);

			builder.Property(x => x.Email)
				.IsRequired();

			builder.Property(x => x.ProvinceId)
				.IsRequired();

			builder.Property(x => x.CountryId)
				.IsRequired();
		}
	}
}
