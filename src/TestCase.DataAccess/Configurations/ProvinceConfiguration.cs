﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TestCase.Domain.Entities;

namespace TestCase.DataAccess.Configurations
{
	public class ProvinceConfiguration : IEntityTypeConfiguration<Province>
	{
		public void Configure(EntityTypeBuilder<Province> builder)
		{
			builder.HasKey(x => x.Id);

			builder.Property(x => x.Name)
				.HasMaxLength(100)
				.IsRequired();
		}
	}
}
