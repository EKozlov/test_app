﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TestCase.Authorization.Token.Provider;
using TestCase.Domain.Entities;
using TestCase.Domain.Entities.OpenIdict;

namespace TestCase.DataAccess
{
	public class ApplicationDbContext : IdentityDbContext<User, Role, Guid>
	{
		public DbSet<OpenIddictApplication> OpenIddictApplication { get; set; }

		public DbSet<OpenIddictAuthorization> OpenIddictAuthorization { get; set; }

		public DbSet<OpenIddictScope> OpenIddictScope { get; set; }

		public DbSet<OpenIddictToken> OpenIddictToken { get; set; }

		public ApplicationDbContext(DbContextOptions options) : base(options)
		{

		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			var countries = SeedDataInitializer.PrepareCountrySeedData();
			
			modelBuilder.Entity<Domain.Entities.Country>()
				.HasData(countries);

			modelBuilder.Entity<Domain.Entities.Province>()
				.HasData(SeedDataInitializer.MapProvinces(countries));

			modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);
		}
	}
}