﻿namespace TestCase.DataAccess
{
	public class SeedDataInitializer
	{
		public static Domain.Entities.Country[] PrepareCountrySeedData()
			=> new Domain.Entities.Country[]
			{
				new ()
				{
					Name = "Country 1",
				},
				new ()
				{
					Name = "Country 2",
				}
			};

		public static IReadOnlyCollection<Domain.Entities.Province> MapProvinces(Domain.Entities.Country[] countries)
		{
			var provinces = new List<Domain.Entities.Province>();

			foreach (var country in countries)
			{
				provinces.AddRange(PrepareProvinces(country.Id));
			}

			return provinces;
		}
		public static Domain.Entities.Province[] PrepareProvinces(Guid countryId)
		{
			return new Domain.Entities.Province[]
			{
				new ()
				{
					Name = "Province 1",
					CountryId = countryId
				},
				new ()
				{
					Name = "Province 2",
					CountryId = countryId
				}
			};
		}
	}
}
