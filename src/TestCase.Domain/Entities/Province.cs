﻿using TestCase.Domain.Base;

namespace TestCase.Domain.Entities
{
	public class Province : BaseEntity
	{
		public string Name { get; set; }
		public Guid CountryId { get; set; }
	}
}
