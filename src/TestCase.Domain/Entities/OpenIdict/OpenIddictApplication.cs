﻿using OpenIddict.EntityFrameworkCore.Models;

namespace TestCase.Domain.Entities.OpenIdict
{
	public class OpenIddictApplication : OpenIddictEntityFrameworkCoreApplication<string, OpenIddictAuthorization,
        OpenIddictToken>
    {
        public OpenIddictApplication()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
