﻿using OpenIddict.EntityFrameworkCore.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestCase.Domain.Entities.OpenIdict
{
	public class OpenIddictAuthorization : OpenIddictEntityFrameworkCoreAuthorization<string, OpenIddictApplication, OpenIddictToken>
    {
        public OpenIddictAuthorization()
        {
            Id = Guid.NewGuid().ToString();
        }

        public string ApplicationId { get; protected set; }

        [ForeignKey(nameof(ApplicationId))]
        public override OpenIddictApplication Application
        {
            get => base.Application;
            set => base.Application = value;
        }
    }
}
