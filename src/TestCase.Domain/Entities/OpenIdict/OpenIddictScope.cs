﻿using OpenIddict.EntityFrameworkCore.Models;

namespace TestCase.Domain.Entities.OpenIdict
{
	public class OpenIddictScope : OpenIddictEntityFrameworkCoreScope<string>
    {
        public OpenIddictScope()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}
