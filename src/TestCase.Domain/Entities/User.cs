﻿using Microsoft.AspNetCore.Identity;
using System.Security.Cryptography;

namespace TestCase.Domain.Entities
{
	public class User : IdentityUser<Guid>
	{
		public string Email { get; set; }
		public Country Country { get; set; }
		public Guid CountryId { get; set; }
		public Province Province { get; set; }
		public Guid ProvinceId { get; set; }
        public string PasswordSalt { get; set; }

        public void HashPassword(string password)
        {
            UpdatePassword(password);
        }

        public bool CheckPassword(string hashedPassword, string password)
        {
            return hashedPassword == GetHashedPassword(password);
        }

        private void UpdatePassword(string password)
        {
            var arrayLenght = 128 / 8;
            var salt = new byte[arrayLenght];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            PasswordSalt = Convert.ToBase64String(salt);
            PasswordHash = GetHashedPassword(password);
        }

        private string GetHashedPassword(string password)
        {
            using var hasher = new Rfc2898DeriveBytes(password, Convert.FromBase64String(PasswordSalt));
            return Convert.ToBase64String(hasher.GetBytes(256 / 8));
        }

    }
}
