﻿using TestCase.Domain.Base;

namespace TestCase.Domain.Entities
{
	public class Country : BaseEntity
	{
		public string Name { get; set; }
		public IEnumerable<Province> Provinces { get; set; } = new List<Province>();
	}
}
