﻿using Microsoft.AspNetCore.Identity;

namespace TestCase.Authorization.Token.Provider
{
	public class Role : IdentityRole<Guid>
    {
        public Role(string roleName) : base(roleName)
        {
        }

        protected Role()
        {
        }
    }
}
