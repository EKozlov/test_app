﻿namespace TestCase.Domain.Base
{
	public class BaseEntity
	{
		public Guid Id { get; private set; } = Guid.NewGuid();
		public DateTimeOffset CreatedDate { get; private set; } = DateTimeOffset.Now;
	}
}
