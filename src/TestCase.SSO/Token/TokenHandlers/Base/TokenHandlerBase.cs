﻿using Microsoft.AspNetCore.Identity;
using OneOf;
using OpenIddict.Abstractions;
using System.Collections.Immutable;
using System.Security.Claims;
using TestCase.Domain.Entities;
using IAccessTokenClaimsProvider = TestCase.Authorization.Token.Provider.IAccessTokenClaimsProvider;

namespace TestCase.Authorization.Token.TokenHandlers.Base
{
	public abstract class TokenHandlerBase : ITokenHandler
	{
		private readonly IAccessTokenClaimsProvider _accessTokenClaimsProvider;
		protected readonly UserManager<User> UserManager;

		public TokenHandlerBase(IAccessTokenClaimsProvider accessTokenClaimsProvider, UserManager<User> userManager)
		{
			_accessTokenClaimsProvider = accessTokenClaimsProvider;
			UserManager = userManager;
		}

		public async Task<OneOf<OpenIdctError, ClaimsPrincipal, bool>> Handle(OpenIddictRequest request)
		{
			if (!request.IsPasswordGrantType() || !IsNeedToHandle(request.GetScopes()))
			{
				return false;
			}

			var user = await GetCurrentUser(request.Username!);

			if (user is null)
			{
				return new UserNotFound("User not found.");
			}


			var isPasswordCorrect = await IsPasswordCorrect(user, request.Password!);

			if (isPasswordCorrect.IsT0)
			{
				return isPasswordCorrect.AsT0;
			}

			return await _accessTokenClaimsProvider.GetUserClaimsPrincipalAsync(user, request.GetScopes());
		}

		protected virtual async Task<OneOf<IncorrectPassword, bool>> IsPasswordCorrect(User user, string password)
		{
			var isCorrectPassword = await UserManager.CheckPasswordAsync(user, password);

			if (!isCorrectPassword)
				return new IncorrectPassword("The username/password couple is invalid.");


			return true;
		}

		protected abstract Task<User?> GetCurrentUser(string userName);
		protected abstract bool IsNeedToHandle(ImmutableArray<string> scopes);
	}

	public class UserNotFound : OpenIdctError
	{
		public UserNotFound(string message) : base(message)
		{
		}
	}

	public class IncorrectPassword : OpenIdctError
	{
		public IncorrectPassword(string message) : base(message)
		{
		}
	}
}
