﻿namespace TestCase.Authorization.Token.TokenHandlers.Base
{
	public class OpenIdctError
	{
		public OpenIdctError(string message)
		{
			Message = message;
		}

		public string Message { get; }
	}
}
