﻿
using OneOf;
using OpenIddict.Abstractions;
using System.Security.Claims;

namespace TestCase.Authorization.Token.TokenHandlers.Base
{
	public interface ITokenHandler
	{
		Task<OneOf<OpenIdctError, ClaimsPrincipal, bool>> Handle(OpenIddictRequest request);
	}
}
