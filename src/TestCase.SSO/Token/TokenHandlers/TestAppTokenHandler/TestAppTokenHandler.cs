﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Immutable;
using TestCase.Authorization.Token.TokenHandlers.Base;
using TestCase.Domain.Entities;

namespace TestCase.Authorization.Token.TokenHandlers.TestAppTokenHandler
{
	public class TestAppTokenHandler : TokenHandlerBase
	{
		public TestAppTokenHandler(
			Provider.IAccessTokenClaimsProvider accessTokenClaimsProvider,
			UserManager<User> userManager)
			: base(accessTokenClaimsProvider, userManager)
		{
		}

		protected override async Task<User?> GetCurrentUser(string userName)
		{
			return await UserManager.FindByNameAsync(userName);
		}
		protected override bool IsNeedToHandle(ImmutableArray<string> scopes)
		{
			return scopes.Contains(ClientsConstants.TestAppScope);
		}
	}
}
