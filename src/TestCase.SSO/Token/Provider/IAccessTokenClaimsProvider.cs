﻿using System.Collections.Immutable;
using System.Security.Claims;
using TestCase.Domain.Entities;

namespace TestCase.Authorization.Token.Provider
{
	public interface IAccessTokenClaimsProvider
	{
		Task<ClaimsPrincipal> GetUserClaimsPrincipalAsync(User user, ImmutableArray<string> scopes);
	}
}
