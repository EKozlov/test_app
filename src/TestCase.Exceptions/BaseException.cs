﻿using Serilog;
using System.Buffers;
using System.Net;
using System.Text.Json;
using TestCase.Resources;

namespace TestCase.Exceptions
{
	public abstract class BaseException : Exception
	{
		private string _message;
		protected BaseException(string message) : base(string.IsNullOrEmpty(message) ? ExceptionMessages.UnknowError : message)
		{
			_message = message;
		}


		public void Serialize(IBufferWriter<byte> output, bool isProduction)
		{
			using var writer = new Utf8JsonWriter(output);
			var serializeOptions = new JsonSerializerOptions
			{
				PropertyNamingPolicy = JsonNamingPolicy.CamelCase
			};

			JsonSerializer.Serialize(writer, new ErrorMessage
			{
				Message = isProduction ? _message : ToString(),
			}, serializeOptions);
			writer.Flush();
		}

		public virtual HttpStatusCode StatusCode => HttpStatusCode.InternalServerError;

		public virtual void WriteToLog(ILogger logger)
		  => logger?.Error(this, Message);
	}
}
