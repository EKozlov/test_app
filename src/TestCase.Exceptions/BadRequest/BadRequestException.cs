﻿using System.Net;

namespace TestCase.Exceptions.BadRequest
{
	public class BadRequestException : BaseException
	{
		public BadRequestException(string message) : base(message)
		{
		}

		public override HttpStatusCode StatusCode => HttpStatusCode.BadRequest;

	}
}
