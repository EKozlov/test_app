﻿namespace TestCase.Exceptions.BadRequest
{
	public class UserAlreadyExistBadRequestException : BadRequestException
	{
		public UserAlreadyExistBadRequestException(string message) : base(message)
		{
		}
	}
}
